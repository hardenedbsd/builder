#!/usr/bin/env zsh
#-
# Copyright (c) 2023-2025 The HardenedBSD Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# Author: Shawn Webb <shawn.webb@hardenedbsd.org>

set -ex

MYSELF="${0}"
TOPDIR="$(dirname $(realpath ${MYSELF}))"

GIT="/usr/local/bin/git"
RSYNC="/usr/local/bin/rsync"
SSH_KEYGEN="/usr/bin/ssh-keygen"
SCP="/usr/bin/scp"

CONFIGDIR="${TOPDIR}/config"
BUILDER_TMPDIR="/data/build/tmp"
CONFIG=""
LOCKFILE=""

BUILD_INSTALLERS=1
BUILD_UPDATE=1

PUBLISH_INSTALLERS=1
PUBLISH_UPDATE=1
PUBLISH_UPDATE_DNS=1

INSTALLER_PUBDIR=""
UPDATE_PUBDIR=""

SIGNED=1
SSH_KEY=""

OBJDIR_TMPFS=""

NJOBS=$(sysctl -n hw.ncpu)

. ${TOPDIR}/lib/git.zsh
. ${TOPDIR}/lib/installers.zsh
. ${TOPDIR}/lib/dns.zsh
. ${TOPDIR}/lib/updater.zsh
. ${TOPDIR}/lib/util.zsh

while getopts "C:c:" o; do
	case "${o}" in
		C)
			CONFIGDIR="${OPTARG}"
			;;
		c)
			CONFIG="${OPTARG}"
			;;
	esac
done

if [ -z "${CONFIG}" ]; then
	echo "[-] Required argument: -c config" >&2
	exit 1
fi

if [ ! -f "${CONFIGDIR}/${CONFIG}" ]; then
	echo "[-] ${CONFIGDIR}/${CONFIG} missing" >&2
	exit 1
fi

. ${CONFIGDIR}/${CONFIG}

check_sanity || exit ${?}

lock_build || exit ${?}
prep_build || cleanup ${?} 1
fetch_src || cleanup ${?} 1

build_update || cleanup ${?} 1
build_src || cleanup ${?} 1
build_installers || cleanup ${?} 1

sign_installers || cleanup ${?} 1
sign_update || cleanup ${?} 1

publish_update || cleanup ${?} 1
publish_installers || cleanup ${?} 1

cleanup
exit ${?}
