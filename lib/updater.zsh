#-
# Copyright (c) 2023-2025 The HardenedBSD Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# Author: Shawn Webb <shawn.webb@hardenedbsd.org>

RSYNC="/usr/local/bin/rsync"

function create_update_config() {
	local unsigned

	if [ ${SIGNED} -gt 0 ]; then
		unsigned=0
	else
		unsigned=1
	fi
	cat<<EOF>${BUILDER_TMPDIR}/${NAME}.hbsd-update-build.conf
BRANCH="${BRANCH}"
INTEGRIFORCE=0
STAGEDIR="${UPDATE_ROOTDIR}/stage"
CHROOTDIR="${UPDATE_ROOTDIR}/chroot"
LOGDIR="${UPDATE_ROOTDIR}/logs"
OUTPUTDIR="${UPDATE_OUTPUTDIR}"
JOBS=${NJOBS}
UNSIGNED=${unsigned}
BUILDSRCSET=0
EOF

	mkdir -p ${UPDATE_ROOTDIR}/stage/../chroot

	return 0
}

function check_build_succeeded() {
	local res

	res="${1}"

	res=$(echo "${res}" | awk '{print $1;}')
	case "${res}" in
		OK)
			return 0
			;;
	esac

	return 1
}

function get_build_version() {
	local build_status
	local res

	build_status="${1}"

	echo "${build_status}" | awk '{print $2;}'
	res=${?}
	if [ ${res} -gt 0 ]; then
		return 1
	fi

	return 0
}

function build_update() {
	local build_version
	local build_status
	local res

	if [ ${BUILD_UPDATE} -eq 0 ]; then
		return 0
	fi

	create_update_config

	build_status=$(hbsd-update-build -c ${BUILDER_TMPDIR}/${NAME}.hbsd-update-build.conf)
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	check_build_succeeded "${build_status}"
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	get_build_version "${build_status}" > ${UPDATE_OUTPUTDIR}/update-latest.txt
	res=${?}
	if [ ${res} -gt 0 ]; then
		return 1
	fi

	return 0
}

function sign_update() {
	local update_file
	local res

	if [ ${SIGNED} -eq 0 ]; then
		return 0
	fi

	update_file="${UPDATE_OUTPUTDIR}/update-latest.txt"
	if [ ! -f ${update_file} ]; then
		echo "[-] Update file ${update_file} not found"
		return 1
	fi

	sign_file "${update_file}"
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	update_file=$(cat ${update_file} | awk -F '|' '{print $2;}')
	update_file="${UPDATE_OUTPUTDIR}/update-${update_file}.tar"
	if [ ! -f ${update_file} ]; then
		echo "[-] Update file ${update_file} not found"
		return 1
	fi

	sign_file "${update_file}"
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	return 0
}

function publish_update_to_main() {
	local latest_file
	local update_file
	local remote_dir

	update_file="${1}"
	latest_file="${2}"
	remote_dir="${3}"

	${SCP} \
		${=SCP_FLAGS} \
		${update_file} \
		${latest_file} \
		${UPDATE_PUB_USER}@hardenedbsd.org:${remote_dir}/

	return ${?}
}

function publish_update() {
	local update_file
	local vernum
	local fname
	local res

	vernum=$(get_next_build_id ${UPDATE_PUBDIR})
	update_file="${UPDATE_OUTPUTDIR}/update-latest.txt"
	if [ ! -f ${update_file} ]; then
		echo "[-] Update file ${update_file} not found"
		return 1
	fi

	update_file=$(cat ${update_file} | awk -F '|' '{print $2;}')
	fname="update-${update_file}.tar"

	update_file="${UPDATE_OUTPUTDIR}/${fname}"
	if [ ! -f ${update_file} ]; then
		echo "[-] Update file ${update_file} not found"
		return 1
	fi

	mkdir -p ${UPDATE_PUBDIR}/build-${vernum}
	mv ${update_file} ${UPDATE_PUBDIR}/build-${vernum}/
	mv ${UPDATE_OUTPUTDIR}/update-latest.txt ${UPDATE_PUBDIR}/build-${vernum}/
	if [ ${SIGNED} -gt 0 ]; then
		mv ${update_file}.sig ${UPDATE_PUBDIR}/build-${vernum}/
		mv ${UPDATE_OUTPUTDIR}/update-latest.txt.sig ${UPDATE_PUBDIR}/build-${vernum}/
	fi

	cache_build_id ${UPDATE_PUBDIR} ${vernum}
	rm -f ${UPDATE_PUBDIR}/LATEST
	ln -sf build-${vernum} ${UPDATE_PUBDIR}/LATEST
	chmod -R 755 ${UPDATE_PUBDIR}/build-${vernum}
	chmod 755 ${UPDATE_PUBDIR}/LATEST

	${RSYNC} -rl ${UPDATE_PUBDIR}/build-${vernum} ${REMOTE_PUB}/update
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi
	${RSYNC} -l ${UPDATE_PUBDIR}/LATEST ${REMOTE_PUB}/update/LATEST
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	if [ ${PUBLISH_UPDATE} -eq 1 ]; then
		update_file="${UPDATE_PUBDIR}/build-${vernum}/${fname}"
		publish_update_to_main \
			${update_file} \
			${UPDATE_PUBDIR}/build-${vernum}/update-latest.txt \
			${UPDATE_MAIN_PUB_DIR}
		res=${?}
		if [ ${res} -gt 0 ]; then
			return ${res}
		fi

		update_dns_record ${DNS_RECORD_NAME} "$(cat ${UPDATE_PUBDIR}/build-${vernum}/update-latest.txt)"
		res=${?}
		if [ ${res} -gt 0 ]; then
			return ${res}
		fi
	fi

	return 0
}
