#-
# Copyright (c) 2023-2025 The HardenedBSD Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# Author: Shawn Webb <shawn.webb@hardenedbsd.org>

function check_sanity() {
	if [ -z "${LOCKFILE}" ]; then
		echo "[-] LOCKFILE required" >&2
		return 1
	fi

	if [ -f "${LOCKFILE}" ]; then
		echo "[-] Build locked. Remove ${LOCKFILE}." >&2
		return 1
	fi

	if [ $(id -u) -gt 0 ]; then
		echo "[-] Must be run as root" >&2
		return 1
	fi

	if [ -z "${SRCDIR}" ]; then
		echo "[-] SRCDIR required" >&2
		return 1
	fi

	if [ ! -d ${SRCDIR} ]; then
		echo "[] ${SRCDIR} not a directory" >&2
		return 1
	fi

	if [ -z "${BRANCH}" ]; then
		echo "[-] BRANCH required" >&2
		return 1
	fi

	if [ ! -x ${GIT} ]; then
		echo "[-] ${GIT} not found" >&2
		return 1
	fi

	if [ ${SIGNED} -gt 0 ]; then
		if [ -z "${SSH_KEY}" ]; then
			echo "[-] Signed build request, but SSH_KEY not specified" >&2
			return 1
		fi

	       	if [ ! -f "${SSH_KEY}" ]; then
			echo "[-] SSH key \"${SSH_KEY}\" not found" >&2
			return 1
		fi

		if [ ! -x "${SSH_KEYGEN}" ]; then
			echo "[-] ${SSH_KEYGEN} not found" >&2
			return 1
		fi
	fi

	if [ ${PUBLISH_INSTALLERS} -gt 0 ]; then
		if [ -z "${INSTALLER_PUBDIR}" ]; then
			echo "[-] Installer publishing requested, but INSTALLER_PUBDIR not specified" >&2
			return 1
		fi

		if [ ! -d "${INSTALLER_PUBDIR}" ]; then
			echo "[-] INSTALLER_PUBDIR \"${INSTALLER_PUBDIR}\" not found" >&2
			return 1
		fi
	fi

	if [ ${PUBLISH_UPDATE} -gt 0 ]; then
		if [ -z "${UPDATE_PUBDIR}" ]; then
			echo "[-] Update publishing requested, but UPDATE_PUBDIR not specified" >&2
			return 1
		fi

		if [ ! -d "${UPDATE_PUBDIR}" ]; then
			echo "[-] UPDATE_PUBDIR \"${UPDATE_PUBDIR}\" not found" >&2
			return 1
		fi
	fi

	return 0
}

function lock_build() {
	touch ${LOCKFILE}
	return ${?}
}

function unlock_build() {
	rm -f ${LOCKFILE}
	return ${?}
}

function prep_build() {
	if [ ! -z "${OBJDIR_TMPFS}" ]; then
		if [ ${BUILD_INSTALLERS} -gt 0 ] && [ ${PUBLISH_INSTALLERS} -gt 0 ]; then
			umount -f ${OBJDIR_TMPFS} || true
			mkdir -p ${OBJDIR_TMPFS}
			mount -t tmpfs tmpfs ${OBJDIR_TMPFS}
		fi
	fi
}

function cleanup() {
	local shouldexit
	local res

	if [ ! -z "${OBJDIR_TMPFS}" ]; then
		if [ ${BUILD_INSTALLERS} -gt 0 ] && [ ${PUBLISH_INSTALLERS} -gt 0 ]; then
			umount ${OBJDIR_TMPFS}
			rm -rf ${OBJDIR_TMPFS}
		fi
	fi

	if [ ! -z "${UPDATE_OUTPUTDIR}" ] && [ -d ${UPDATE_OUTPUTDIR} ]; then
		find ${UPDATE_OUTPUTDIR} -type f | xargs rm -f
	fi

	unlock_build
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	shouldexit="${1}"
	if [ ! -z "${shouldexit}" ]; then
		exit ${shouldexit}
	fi

	return 0
}

function get_objdir() {
	if [ ! -z "${OBJDIR_TMPFS}" ]; then
		echo ${OBJDIR_TMPFS}
		return 0
	fi

	make -C ${SRCDIR} -V .OBJDIR buildworld
	return ${?}
}

function get_latest_build_id() {
	local publishdir
	local vernum

	publishdir="${1}"
	if [ -z "${publishdir}" ]; then
		echo "[-] ${0}: specify publish directory" >&2
		return 0
	fi

	if [ -f ${publishdir}/index.txt ]; then
		vernum=$(cat ${publishdir}/index.txt)
		echo ${vernum}
		return ${vernum}
	fi

	echo 1
	return 0
}

function get_next_build_id() {
	local vernum

	vernum=$(get_latest_build_id "${1}")
	if [ ${vernum} -eq 0 ]; then
		echo 0
		return 0
	fi

	vernum=$((${vernum} + 1))
	echo ${vernum}
	return 0
}

function cache_build_id() {
	local fulldir
	local pubdir
	local vernum
	local res

	pubdir="${1}"
	vernum=${2}
	fulldir="${3}"

	if [ -z "${pubdir}" ] || [ -z "${vernum}" ]; then
		return 1
	fi

	echo -n ${vernum} > ${pubdir}/index.txt
	chmod 755 ${pubdir}/index.txt

	if [ ! -z "${fulldir}" ]; then
		ln -sf ${fulldir} ${pubdir}/LATEST
		res=${?}
		if [ ${res} -gt 0 ]; then
			return ${res}
		fi
	fi

	return 0
}

function sign_file() {
	local f

	f="${1}"

	if [ -z "${f}" ] || [ ! -f ${f} ]; then
		return 1
	fi

	yes | ${SSH_KEYGEN} -Y sign -f ${SSH_KEY} -n file ${f}
	return ${?}
}

function sign_directory() {
	local pubdir
	local res
	local f

	pubdir="${1}"
	if [ -z "${pubdir}" ]; then
		echo "[-] ${0}: Specify publish directory" >&2
		return 1
	fi

	(
		cd ${pubir}
		for f in $(find ${pubdir} -maxdepth 1 -type f -a ! -name \*.sig); do
			sign_file ${f}
			res=${?}
			if [ ${res} -gt 0 ]; then
				exit ${res}
			fi
		done
	)

	return 0
}
