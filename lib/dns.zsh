#!/usr/local/bin/zsh
#-
# Copyright (c) 2018 HardenedBSD
# Author: Shawn Webb <shawn.webb@hardenedbsd.org>
#
# This work originally sponsored by G2, Inc
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

DNS_API_URL="https://api.cloudflare.com/client/v4"

function get_dns_zoneid() {
	local name

	name="${1}"

	curl -s -X GET "${DNS_API_URL}/zones?name=${name}" \
		-H "Authorization: Bearer ${DNS_API_KEY}" \
		-H "Content-Type: application/json" | \
		jq -r '.result[0].id'
	return ${?}
}

function get_dns_recordid() {
	local recordname
	local zoneid

	recordname="${1}"
	zoneid="${2}"

	curl -s -X GET "${DNS_API_URL}/zones/${zoneid}/dns_records?name=${recordname}" \
		-H "Authorization: Bearer ${DNS_API_KEY}" \
		-H "Content-Type: application/json" | \
		jq -r '.result[0].id'
	return ${?}
}

function generate_new_dns_record_data() {
	local recordname
	local buildver
	local recordid
	local zonename
	local zoneid

	recordid="${1}"
	recordname="${2}"
	zoneid="${3}"
	buildver="${4}"
	zonename="${5}"

	cat<<EOF
{
	"id": "${recordid}",
	"type": "TXT",
	"name": "${recordname}",
	"content": "${buildver}",
	"proxiable": false,
	"proxied": false,
	"ttl": 3600,
	"locked": false,
	"zoneid": "${zoneid}",
	"zone_name": "${zonename}",
	"data": {}
}
EOF
	return 0
}

function update_dns_record() {
	local buildver
	local recordid
	local zonename
	local tmpfile
	local zoneid
	local res

	if [ ${PUBLISH_UPDATE_DNS} -eq 0 ]; then
		return 0
	fi

	recordname="${1}"
	buildver="${2}"

	zonename="hardenedbsd.org"
	zoneid=$(get_dns_zoneid ${zonename})
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	recordid=$(get_dns_recordid ${DNS_RECORD_NAME} ${zoneid})
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	tmpfile=$(mktemp)
	generate_new_dns_record_data \
		${recordid} \
		${recordname} \
		${zoneid} \
		${buildver} \
		${zonename} > ${tmpfile}

	curl -s -X PUT "${DNS_API_URL}/zones/${zoneid}/dns_records/${recordid}" \
		-H "Authorization: Bearer ${DNS_API_KEY}" \
		-H "Content-Type: application/json" \
		--data "@${tmpfile}"
	res=${?}

	echo "==== ==== ==== ===="
	cat ${tmpfile}
	echo "==== ==== ==== ===="

	rm -f ${tmpfile}
	return ${res}
}
