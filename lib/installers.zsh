#-
# Copyright (c) 2023-2025 The HardenedBSD Project
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# Author: Shawn Webb <shawn.webb@hardenedbsd.org>

function build_src() {
	if [ ${BUILD_INSTALLERS} -eq 0 ]; then
		return 0
	fi

	(
		set -ex

		cd ${SRCDIR}
		make \
			-j ${NJOBS} \
			buildworld \
			buildkernel
	)

	return ${?}
}

function compress_installers() {
	local objdir
	local f

	(
		objdir="$(get_objdir)/release"
		cd ${objdir}
		for f in $(find ${objdir} -name \*.img -o -name \*.iso); do
			xz -T ${NJOBS} -9 -k ${f}
			res=${?}
			if [ ${res} -gt 0 ]; then
				exit ${res}
			fi
		done
		exit 0
	)

	return ${?}
}

function build_installers() {
	local res

	if [ ${BUILD_INSTALLERS} -eq 0 ]; then
		return 0
	fi

	(
		set -ex

		cd ${SRCDIR}/release
		make clean
		make obj
		make real-release
		exit ${?}
	)
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${?}
	fi

	compress_installers

	return ${?}
}

function sign_installers() {
	local objdir
	local res

	if [ ${SIGNED} -eq 0 ]; then
		return 0
	fi

	objdir="$(get_objdir)/release"
	sign_directory ${objdir}
	res=${?}
	if [ ${res} -gt 0 ]; then
		return 1
	fi

	return 0
}

function publish_installers() {
	local objdir
	local pubdir
	local vernum
	local res

	if [ ${PUBLISH_INSTALLERS} -eq 0 ]; then
		return 0
	fi

	vernum=$(get_next_build_id ${INSTALLER_PUBDIR})

	objdir=$(get_objdir)
	pubdir="${INSTALLER_PUBDIR}/build-${vernum}"
	mkdir -p ${pubdir}

	${RSYNC} -a --exclude '/*/*' ${objdir}/release/ ${pubdir}
	res=${?}
	if [ ${res} -gt 0 ]; then
		return ${res}
	fi

	cache_build_id ${INSTALLER_PUBDIR} ${vernum}
	rm -f ${INSTALLER_PUBDIR}/LATEST
	ln -sf build-${vernum} ${INSTALLER_PUBDIR}/LATEST

	chmod -R 755 ${pubdir}

	${RSYNC} -rl ${pubdir} ${REMOTE_PUB}/installer
	${RSYNC} -l ${INSTALLER_PUBDIR}/LATEST ${REMOTE_PUB}/installer

	return 0
}
